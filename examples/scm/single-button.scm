;;Created by guile-glade.
;;Ariel Rios 1999

(use-modules (gtk gtk) (gtk gdk))

(define (project)   
(let* ((window1  (gtk-window-new 'dialog))
	(vbox1(gtk-vbox-new #f 0))
	(button1 (gtk-button-new-with-label "button1")) )

	;;widget properties
  
	(gtk-window-set-title window1 "window1") 
	(gtk-container-add window1 vbox1)
	(gtk-box-pack-start vbox1 button1 #f #f)

	;;Here you should put your main code 
	;;Please beware that if you rebuild your gui, 
	;;this code will be erased.

        (gtk-widget-show-all window1)
 
	))  

(project)
