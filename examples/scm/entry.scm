;;Created by guile-glade.
;;Ariel Rios 1999

(use-modules (gtk gtk) (gtk gdk))

(define (project1)   
(let* ((window1  (gtk-window-new 'dialog))
	(vbox1(gtk-vbox-new #t 0))
	(entry1 (gtk-entry-new)) 
)

	;;widget properties
  
	(gtk-window-set-title window1 "window1") 
	(gtk-container-add window1 vbox1)
	(gtk-box-pack-start vbox1 entry1 #t #f)

	;;Here you should put your main code 
	;;Please beware that if you rebuild your gui, 
	;;this code will be erased.
 
	))  