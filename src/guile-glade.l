%{
  /*
    Ariel Rios Osorio
    jarios@usa.net
    http://erin.netpedia.net
  */

  //Licensed under GPL

#include "guile-glade.yy.tab.h"
  
%}
 
%%
[\t\n ]+ 

"<?xml version=\"1.0\"?>" { return VERSION; }

"<GTK-Interface>" { return HEAD; }

"<project>" { return PROJECT_OPEN; }

"<name>" { return NAME_OPEN; } 			

"</name>" { return NAME_CLOSE; }

"<program_name>" { return PROGRAM_NAME_OPEN; } 	     

"</program_name>" { return PROGRAM_NAME_CLOSE; }

"<directory>" { return DIRECTORY_OPEN; }		

"</directory>" { return DIRECTORY_CLOSE; }

"<source_directory>" { return SOURCE_DIRECTORY_OPEN; } 	 	

"</source_directory>" { return SOURCE_DIRECTORY_CLOSE; }

"<pixmaps_directory>" { return PIXMAPS_DIRECTORY_OPEN; }

"</pixmaps_directory>" { return PIXMAPS_DIRECTORY_CLOSE; }

"<language>" { return LANGUAGE_OPEN; }	

"</language>" { return LANGUAGE_CLOSE; }

"<gnome_support>" { return GNOME_SUPPORT_OPEN; }	       

"</gnome_support>" { return GNOME_SUPPORT_CLOSE; }

"<gettext_support>" { return GETTEXT_SUPPORT_OPEN; }	

"</gettext_support>" { return GETTEXT_SUPPORT_CLOSE; }

"<use_widget_names>" { return USE_WIDGET_NAMES_OPEN; }	

"</use_widget_names>" { return USE_WIDGET_NAMES_CLOSE; }

"<output_main_file>" {	return OUTPUT_MAIN_FILE_OPEN; }

"</output_main_file>" { return OUTPUT_MAIN_FILE_CLOSE; }

"<output_support_files>" { return OUTPUT_SUPPORT_FILES_OPEN; }

"</output_support_files>" { return OUTPUT_SUPPORT_FILES_CLOSE; }

"<output_build_files>" { return	OUTPUT_BUILD_FILES_OPEN; }

"</output_build_files>" { return OUTPUT_BUILD_FILES_CLOSE; }

"<backup_source_files>" { return BACKUP_SOURCE_FILES_OPEN; }

"</backup_source_files>" { return BACKUP_SOURCE_FILES_CLOSE; }

"<main_source_file>" { return MAIN_SOURCE_FILE_OPEN; }	

"</main_source_file>" { return MAIN_SOURCE_FILE_CLOSE; }

"<main_header_file>" { return MAIN_HEADER_FILE_OPEN; }	

"</main_header_file>" { return MAIN_HEADER_FILE_CLOSE; }

"<handler_source_file>" { return HANDLER_SOURCE_FILE_OPEN; }

"</handler_source_file>" { return HANDLER_SOURCE_FILE_CLOSE; }

"<handler_header_file>" { return HANDLER_HEADER_FILE_OPEN; }

"</handler_header_file>" { return HANDLER_HEADER_FILE_CLOSE; }

"<support_source_file>" { return SUPPORT_SOURCE_FILE_OPEN; }

"</support_source_file>" { return SUPPORT_SOURCE_FILE_CLOSE; }

"<support_header_file>" { return SUPPORT_HEADER_FILE_OPEN; }

"</support_header_file>" { return SUPPORT_HEADER_FILE_CLOSE; }

"<translatable_strings_file>" { return TRANSLATABLE_STRINGS_FILE_OPEN; }

"</translatable_strings_file>" { return TRANSLATABLE_STRINGS_FILE_CLOSE; }

"<class>" { return CLASS_OPEN;  }

"</class>" { return CLASS_CLOSE; }

"<can_focus>" { return CAN_FOCUS_OPEN; }

"</can_focus>" { return CAN_FOCUS_CLOSE; }

"<label>" { return LABEL_OPEN; }

"</label>" { return LABEL_CLOSE; }

"<child>" { return CHILD_OPEN; }

"</child>" { return CHILD_CLOSE; }

"<padding>" { return PADDING_OPEN; }

"</padding>" { return PADDING_CLOSE; }

"<expand>" { return EXPAND_OPEN; }

"</expand>" { return EXPAND_CLOSE; }

"<fill>" { return FILL_OPEN; }

"</fill>" { return FILL_CLOSE; }

"<border_width>" { return BORDER_WIDTH_OPEN; }

"</border_width>" { return BORDER_WIDTH_CLOSE; }

"<title>" { return TITLE_OPEN; }

"</title>" { return TITLE_CLOSE; }

"<type>" { return TYPE_OPEN; }

"</type>" { return TYPE_CLOSE; }

"<position>" { return POSITION_OPEN; }

"</position>" { return POSITION_CLOSE; }

"<modal>" { return MODAL_OPEN; }

"</modal>" { return MODAL_CLOSE; }

"<allow_shrink>" { return ALLOW_SHRINK_OPEN; }

"</allow_shrink>" { return ALLOW_SHRINK_CLOSE; }

"<allow_grow>" { return ALLOW_GROW_OPEN; }

"</allow_grow>" { return ALLOW_GROW_CLOSE; }

"<auto_shrink>" { return AUTO_SHRINK_OPEN; }

"</auto_shrink>" { return AUTO_SHRINK_CLOSE; }

"</project>" { return PROJECT_CLOSE; }

"<widget>" { return WIDGET_OPEN; }

"</widget>" { return WIDGET_CLOSE; }

"<homogeneous>" { return HOMOGENEOUS_OPEN; }

"</homogeneous>" { return HOMOGENEOUS_CLOSE; }

"<spacing>" { return SPACING_OPEN; }

"</spacing>" { return SPACING_CLOSE; }

"<editable>" { return EDITABLE_OPEN; }

"</editable>" { return EDITABLE_CLOSE; }

"<text_visible>" { return TEXT_VISIBLE_OPEN; }

"</text_visible>" { return TEXT_VISIBLE_CLOSE; }

"<text_max_length>" { return TEXT_MAX_LENGTH_OPEN; }

"</text_max_length>" { return TEXT_MAX_LENGTH_CLOSE; }

"<text>" { return TEXT_OPEN; }

"</text>" { return TEXT_CLOSE; }

"<pack>" { return PACK_OPEN; }

"</pack>" { return PACK_CLOSE; }

"</GTK-Interface>" { return TAIL; }

"GtkVBox" { 
  yylval.bool = 0; 
  return BOOL;
}

"GtkHBox" {
  yylval.bool = 1;
  return BOOL;
}

True { 
  yylval.bool = 1; 
  return BOOL;
}

False {
  yylval.bool = 0;
  return BOOL; 
}

[a-zA-Z]+[1-9]*"."*[ch]*"scm"* | 
[A-Z]+"_"*[A-Z]*"_"*[A-Z]*"_"*[A-Z]* {
  yylval.string = (char *) malloc(sizeof(char) * strlen(yytext));
  yylval.string = yytext;
  return STRING;
}

[0-9]+ {
 yylval.number = atoi(yytext);
 return NUMBER;
}
.

%%



