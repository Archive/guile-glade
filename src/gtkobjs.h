#include "objs.h"


typedef struct _gtkboxpackinfo{

  char * box;
  bool padding;
  bool expand;

}GtkBoxPackInfo;


typedef struct _gtkwindow{

  char * name;
  char * title;
  char * type;
  char * position;
  int border_width;
  bool modal;
  bool allow_shrink;
  bool allow_grow;
  bool auto_shrink;

}GtkWindow;

typedef struct _gtkbox{
  
  bool class;  //0=>vbox; 1=>hbox
  char * name;
  bool homogeneous;
  short spacing;
  GtkBoxPackInfo * info;

}GtkBox;

typedef struct _gtkbutton{

  char * name;
  char * label; 
  bool can_focus;
  short padding;
  bool expand;
  bool fill;
}GtkButton;

typedef struct _gtkentry {

  char * name;
  char * text;
  char * pack;
  bool can_focus;
  bool editable;
  bool text_visible;
  short text_max_length;
  short padding;
  bool expand;
  bool fill;

}GtkEntry;

typedef struct _gtktext {

  char * name;
  bool can_focus;
  bool editable;
  char * text;
  bool padding;
  bool expand;
  bool fill;

}GtkText;  
  
  








