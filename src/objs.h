//Ariel Rios
//Licensed under the GPL

#include <stdio.h>

#define TRUE 1;
#define FALSE 0;

typedef short bool;

typedef struct property_obj{

  /* We declare the char variables as pointers so we can 
     dynamically assign memory at run time
  */

  char * project_name;
  char * program_name;
  char * directory;
  char * src_dir;
  char * pixmap_dir;
  char * language;

  char * main_source_file;
  char * main_header_file;
  char * handler_source_file;
  char * handler_header_file;
  char * support_source_file;
  char * support_header_file;
  char * translatable_strings_file;

  bool  gnome_support;
  bool  gettext_support;
  bool  use_widgets_names;
  bool  output_main_file;
  bool  output_support_file;
  bool  output_build_files;
  bool  backup_source_files;

 
}Property_Obj;
  













