#include <ctype.h>
#include <stdio.h>
#include <string.h>



#ifndef GTK_LIBRARY
#define GTK_LIBRARY
#include "gtkobjs.h"
#include "properties.h"
#endif



int length_string(char * parse_string){
  
  int i = 0;
  
  while(!isspace(parse_string[i]))
    i++;
  
  return i; }

void save_string(char * str, char * parse_string, int length){

  char * buffer = (char *) malloc(sizeof(char) * length);
  int i;

  for(i = 0; i < length; i++)
    buffer[i] = parse_string[i];

  for(i = 0; i < length; i++)
    str[i] = buffer[i];
  
  
  //printf("Esto es buffer:%s esto es prop:%s", buffer, str);

}

int write_properties(Property_Obj * properties){

  char * buffer;
  int fd;

  fd = creat("main.tmp", 0755);

  write(fd, COPYRIGHT, strlen(COPYRIGHT));  
  
  if(properties->gnome_support)
    write(fd, GNOME, strlen(GNOME));
  else write(fd, GTK_ONLY, strlen(GTK_ONLY));
  
  write(fd, GUILE_DEFINE, strlen(GUILE_DEFINE));
  
  //printf("%s\n", properties->project_name);
  
  write(fd, properties->program_name, strlen(properties->program_name));

  write(fd, ")", 1);

  write(fd, "  ", 2);

  return fd;
}
  
  
  













