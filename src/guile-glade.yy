%{
  /*
    Ariel Rios Osorio
    jarios@usa.net
    http://erin.netpedia.net
  */

  //Licensed under GPL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef GTK_LIBRARY
#define GTK_LIBRARY
#include "gtkobjs.h"
#endif



  extern void append_files(char *[], int);
  extern int length_string(char *);
  extern lookup_widget_type(int, void *);
  extern void save_string(char *, char *, int);
  extern int write_properties(Property_Obj *);
  extern void write_window(void *, int *, char **);
  extern void write_box(void *, int *, char **);
  extern void write_button(void *, int *, char **);  

  
/* 
  fd is the main file to which ev'rything will
   be copied. 
   fd_let will contain a temporary file with all
   the widgets that are going to be binded
   fd_prop will have the functions that set the properties.
   Don't know if thi's the best solution but's the only
   one I have at the moment
*/  

  int fd1; // the big cheese file!!!
  int fd[4];

/* fd[0] is the main output temp file
   fd[1] is the let bindings temp output file
   fd[2] is the widget properties temp output file
   fd[3] is the widget_index 

  int fd_main;
  int fd_let;
  int fd_prop;
  */

  /* widget_index will help us to keep track unto which widget we need to 
     add the widget unto. If we have -1 then we'll do a gtk-container-add
     and on the rest (normally) a gtk-box-pack-start

     widget_list is a linked list queue that will hold the name of the widgets
     upon we will be refered by widget_index.
  */

  char ** widget_list;

  Property_Obj * properties;

  %}

%union {
  char * string;
  int bool;
  int number;
}

%token <string> STRING 
%token <number> NUMBER
%token VERSION HEAD
%token PROJECT_OPEN NAME_OPEN NAME_CLOSE
%token PROGRAM_NAME_OPEN PROGRAM_NAME_CLOSE
%token DIRECTORY_OPEN DIRECTORY_CLOSE
%token SOURCE_DIRECTORY_OPEN SOURCE_DIRECTORY_CLOSE
%token PIXMAPS_DIRECTORY_OPEN PIXMAPS_DIRECTORY_CLOSE
%token LANGUAGE_OPEN LANGUAGE_CLOSE
%token GNOME_SUPPORT_OPEN GNOME_SUPPORT_CLOSE
%token GETTEXT_SUPPORT_OPEN GETTEXT_SUPPORT_CLOSE
%token USE_WIDGET_NAMES_OPEN USE_WIDGET_NAMES_CLOSE
%token OUTPUT_MAIN_FILE_OPEN OUTPUT_MAIN_FILE_CLOSE 
%token OUTPUT_SUPPORT_FILES_OPEN OUTPUT_SUPPORT_FILES_CLOSE
%token OUTPUT_BUILD_FILES_OPEN OUTPUT_BUILD_FILES_CLOSE
%token BACKUP_SOURCE_FILES_OPEN BACKUP_SOURCE_FILES_CLOSE
%token MAIN_SOURCE_FILE_OPEN MAIN_SOURCE_FILE_CLOSE
%token MAIN_HEADER_FILE_OPEN MAIN_HEADER_FILE_CLOSE
%token HANDLER_SOURCE_FILE_OPEN HANDLER_SOURCE_FILE_CLOSE
%token HANDLER_HEADER_FILE_OPEN HANDLER_HEADER_FILE_CLOSE
%token SUPPORT_SOURCE_FILE_OPEN SUPPORT_SOURCE_FILE_CLOSE
%token SUPPORT_HEADER_FILE_OPEN SUPPORT_HEADER_FILE_CLOSE
%token TRANSLATABLE_STRINGS_FILE_OPEN TRANSLATABLE_STRINGS_FILE_CLOSE 
%token WIDGET_OPEN WIDGET_CLOSE 
%token CLASS_OPEN CLASS_CLOSE
%token LABEL_OPEN LABEL_CLOSE
%token CHILD_OPEN CHILD_CLOSE
%token PADDING_OPEN PADDING_CLOSE
%token EXPAND_OPEN EXPAND_CLOSE
%token FILL_OPEN FILL_CLOSE
%token CAN_FOCUS_OPEN CAN_FOCUS_CLOSE
%token BORDER_WIDTH_OPEN BORDER_WIDTH_CLOSE
%token TITLE_OPEN TITLE_CLOSE
%token TYPE_OPEN STRING TYPE_CLOSE
%token POSITION_OPEN POSITION_CLOSE
%token MODAL_OPEN MODAL_CLOSE
%token ALLOW_SHRINK_OPEN ALLOW_SHRINK_CLOSE
%token ALLOW_GROW_OPEN ALLOW_GROW_CLOSE
%token AUTO_SHRINK_OPEN AUTO_SHRINK_CLOSE
%token HOMOGENEOUS_OPEN HOMOGENEOUS_CLOSE
%token SPACING_OPEN SPACING_CLOSE
%token EDITABLE_OPEN EDITABLE_CLOSE
%token TEXT_OPEN TEXT_CLOSE
%token TEXT_VISIBLE_OPEN TEXT_VISIBLE_CLOSE
%token TEXT_MAX_LENGTH_OPEN TEXT_MAX_LENGTH_CLOSE
%token PACK_OPEN PACK_CLOSE
%token PROJECT_CLOSE
%token <bool> BOOL
%token TAIL

%%

file: version head project_options widget tail ;

version : VERSION 
        ;

head : HEAD
     ;

project_options:  
PROJECT_OPEN
NAME_OPEN STRING NAME_CLOSE
PROGRAM_NAME_OPEN STRING PROGRAM_NAME_CLOSE
DIRECTORY_OPEN STRING DIRECTORY_CLOSE
SOURCE_DIRECTORY_OPEN STRING SOURCE_DIRECTORY_CLOSE
PIXMAPS_DIRECTORY_OPEN STRING PIXMAPS_DIRECTORY_CLOSE
LANGUAGE_OPEN STRING LANGUAGE_CLOSE
GNOME_SUPPORT_OPEN BOOL GNOME_SUPPORT_CLOSE
GETTEXT_SUPPORT_OPEN BOOL GETTEXT_SUPPORT_CLOSE
USE_WIDGET_NAMES_OPEN BOOL USE_WIDGET_NAMES_CLOSE
OUTPUT_MAIN_FILE_OPEN BOOL OUTPUT_MAIN_FILE_CLOSE 
OUTPUT_SUPPORT_FILES_OPEN BOOL OUTPUT_SUPPORT_FILES_CLOSE
OUTPUT_BUILD_FILES_OPEN BOOL OUTPUT_BUILD_FILES_CLOSE
BACKUP_SOURCE_FILES_OPEN BOOL BACKUP_SOURCE_FILES_CLOSE
MAIN_SOURCE_FILE_OPEN STRING MAIN_SOURCE_FILE_CLOSE
MAIN_HEADER_FILE_OPEN STRING MAIN_HEADER_FILE_CLOSE
HANDLER_SOURCE_FILE_OPEN STRING HANDLER_SOURCE_FILE_CLOSE
HANDLER_HEADER_FILE_OPEN STRING HANDLER_HEADER_FILE_CLOSE
SUPPORT_SOURCE_FILE_OPEN STRING SUPPORT_SOURCE_FILE_CLOSE
SUPPORT_HEADER_FILE_OPEN STRING SUPPORT_HEADER_FILE_CLOSE
TRANSLATABLE_STRINGS_FILE_OPEN STRING TRANSLATABLE_STRINGS_FILE_CLOSE 
PROJECT_CLOSE
{

  /*
    The whole magic appears and is done at this part.
    GLADE's grammar is a very easy although a large one.
    We' need to use correctly the $n vars to write what we 
    want!
    We assign every $ to the correct Property_Obj member malloquing
    memory first. After we've done this, we send the fd and Property_Obj
    pointer to write_properties who does the magic!
  */ 

 properties = (Property_Obj *) malloc(sizeof(Property_Obj));
 
 properties->project_name = (char *) malloc(sizeof(char) * length_string($3));
 save_string(properties->project_name, $3, length_string($3));

 properties->program_name = (char *) malloc(sizeof(char) * length_string($6));
 save_string(properties->program_name, $6, length_string($6));

 properties->directory = (char *) malloc(sizeof(char) * length_string($9));
 save_string(properties->directory, $9, length_string($9));

 properties->src_dir = (char *) malloc(sizeof(char) * length_string($12));
 save_string(properties->src_dir, $12, length_string($12));
 
 properties->pixmap_dir = (char *) malloc(sizeof(char) * length_string($15));
 save_string(properties->pixmap_dir, $15, length_string($15));

 properties->language = (char *) malloc(sizeof(char) * length_string($18));
 save_string(properties->language, $18, length_string($18));

 properties->main_source_file = (char *) malloc(sizeof(char) * length_string($42));
 save_string(properties->main_source_file, $42, length_string($42));
 
 properties->main_header_file = (char *) malloc(sizeof(char) * length_string($45));
 save_string(properties->main_header_file, $45, length_string($45));
 
 properties-> handler_source_file = (char *) malloc(sizeof(char) * length_string($48));
 save_string(properties->handler_source_file, $48, length_string($48)); 
 
 properties->handler_header_file = (char *) malloc(sizeof(char) * length_string($51));
 save_string(properties->handler_header_file, $51, length_string($51)); 
 
 properties->support_source_file = (char *) malloc(sizeof(char) * length_string($54));
 save_string(properties->support_source_file, $54, length_string($54)); 
 
 properties->support_header_file = (char *) malloc(sizeof(char) * length_string($57));
 save_string(properties->support_header_file, $57 , length_string($57)); 
 
 properties->translatable_strings_file = (char *) malloc(sizeof(char) *length_string($60));
 save_string(properties->translatable_strings_file, $60, length_string($60));

 properties->gnome_support = $21;

 properties->gettext_support = $24;

 properties->use_widgets_names = $27;

 properties->output_main_file = $30;

 properties->output_support_file = $33;
 
 properties->output_build_files = $36;

 properties->backup_source_files = $39;

 /*
   We now call the function  write_properties sending the fd and the pointer to the
    properties struc. Here we are going to write the required files with the require
    attributes
 */

 fd1 = creat(properties->program_name, 0755);
 fd[0] = write_properties(properties);
 fd[1] = creat("let.tmp", 0700);
 fd[2] = creat("prop.tmp", 0700);

 fd[3] = -1;	//widget_index

 write(fd[1], "\n(let* (", 8);

 /* We send to hell properties...*/

 free(properties);

  widget_list = (char **) malloc(sizeof(char **));
  
}
;

widget : WIDGET_OPEN obj WIDGET_CLOSE  
       | WIDGET_OPEN obj widget WIDGET_CLOSE 
      ;

obj : gtkobj 
    ;

gtkobj : gtkwindow 
       | gtkbox
       | gtkbutton
       | gtkentry
       ;


gtkwindow : CLASS_OPEN STRING CLASS_CLOSE
            NAME_OPEN STRING NAME_CLOSE
            BORDER_WIDTH_OPEN NUMBER BORDER_WIDTH_CLOSE
	    TITLE_OPEN STRING TITLE_CLOSE
            TYPE_OPEN STRING TYPE_CLOSE
            POSITION_OPEN STRING POSITION_CLOSE
            MODAL_OPEN BOOL MODAL_CLOSE
            ALLOW_SHRINK_OPEN BOOL ALLOW_SHRINK_CLOSE
            ALLOW_GROW_OPEN BOOL ALLOW_GROW_CLOSE
            AUTO_SHRINK_OPEN BOOL AUTO_SHRINK_CLOSE 
{
  GtkWindow * window;	

  window = (GtkWindow *) malloc(sizeof(GtkWindow));

  window->name = (char *) malloc(sizeof(char *) * length_string($5));
  save_string(window->name, $5, length_string($5));
 
  window->title = (char *) malloc(sizeof(char *) * length_string($11));
  save_string(window->title, $11, length_string($11));

  window->type = (char *) malloc(sizeof(char *) * length_string($14));
  save_string(window->type, $14, length_string($14));

  window->position = (char *) malloc(sizeof(char *) * length_string($17));
  save_string(window->position, $17, length_string($17));

  window->border_width = (short)$8;
  window->modal = (short)$20;
  window->allow_shrink = (short)$23;
  window->allow_grow = (short)$26;
  window->auto_shrink = (short)$29;

  /*
    Dinamycally, we create space for the next content of the widget_list.
  */

  widget_list[++fd[3]] = (char *) malloc(sizeof(char *) * strlen(window->name));
  widget_list[fd[3]] = window->name;

  write_window((void *) window, (int *) fd, (char **) widget_list );

  free(window);
  
}
;

gtkbox : CLASS_OPEN BOOL CLASS_CLOSE
         NAME_OPEN STRING NAME_CLOSE
         HOMOGENEOUS_OPEN BOOL HOMOGENEOUS_CLOSE              
         SPACING_OPEN NUMBER SPACING_CLOSE
{ 
  GtkBox * box;
  
  box = (GtkBox *) malloc(sizeof(GtkBox));

  box->class = $2;
 
  box->name = (char *) malloc(sizeof(char *) * length_string($5));
  save_string(box->name, $5, length_string($5));

  box->homogeneous = $8; 
  box->spacing = $11;

/*
    Same as above. We add the widget name into the stack
    Please note that we only need to push into the stack
    window, boxes or containers. We do not push other widgets
*/


  widget_list[++fd[3]] = (char *) malloc(sizeof(char *) * strlen(box->name));
  widget_list[fd[3]] = box->name;
  
  write_box((void *) box, (int *) fd, (char **) widget_list);
 
  free(box);
      
}
;


gtkbutton : CLASS_OPEN STRING CLASS_CLOSE
            NAME_OPEN  STRING NAME_CLOSE
            CAN_FOCUS_OPEN  BOOL CAN_FOCUS_CLOSE 
            LABEL_OPEN STRING LABEL_CLOSE
            CHILD_OPEN
            PADDING_OPEN NUMBER PADDING_CLOSE
            EXPAND_OPEN BOOL EXPAND_CLOSE
            FILL_OPEN BOOL FILL_CLOSE
            CHILD_CLOSE
{
 
  GtkButton * button;

  button = (GtkButton *) malloc(sizeof(GtkButton));

  button->name = (char *) malloc(sizeof(char *) * length_string($5));
  save_string(button->name, $5, length_string($5));

  button->label = (char *) malloc(sizeof(char *) * length_string($11));
  save_string(button->label, $11, length_string($11));
  
  button->can_focus = $8;
  button->padding = $15;
  button->expand = $18;
  button->fill = $21;
  
  write_button((void *) button, (int *) fd, (char **) widget_list);

  free(button);
}
;

gtkentry : CLASS_OPEN STRING CLASS_CLOSE
           NAME_OPEN STRING NAME_CLOSE
           CAN_FOCUS_OPEN BOOL CAN_FOCUS_CLOSE
           EDITABLE_OPEN BOOL EDITABLE_CLOSE
           TEXT_VISIBLE_OPEN BOOL TEXT_VISIBLE_CLOSE
           TEXT_MAX_LENGTH_OPEN NUMBER TEXT_MAX_LENGTH_CLOSE
           TEXT_OPEN TEXT_CLOSE
           CHILD_OPEN
           PADDING_OPEN NUMBER PADDING_CLOSE
           EXPAND_OPEN BOOL EXPAND_CLOSE
           FILL_OPEN BOOL FILL_CLOSE
           PACK_OPEN STRING PACK_CLOSE
           CHILD_CLOSE
{
  GtkEntry * entry;

  entry = (GtkEntry *) malloc(sizeof(GtkEntry));

  entry->name = (char *) malloc(sizeof(char *) * length_string($5));
  save_string(entry->name, $5, length_string($5));
  
  entry->can_focus = $8;
  entry->editable = $11;
  entry->text_visible = $14;
  entry->text_max_length = $17;
  entry->padding = $23;
  entry->expand = $26;
  entry->fill = $29;

  write_entry((void *) entry, (int *) fd, (char **) widget_list);

  free(entry);

}
;

tail: TAIL 
{
  char *files[]={ "main.tmp", "let.tmp", "prop.tmp" };
  int i;

  write(fd[1], ")\n", 2);
  write(fd[1], "\n\t;;widget properties\n ", 23);
 
  write(fd[2], "\n\n\t;;Here you should put your main code ", 40);
  write(fd[2], "\n\t;;Please beware that if you rebuild your gui, ", 48);
  write(fd[2], "\n\t;;this code will be erased.\n ", 31);
  write(fd[2], "\n\t)) ", 5);   

 close(fd[0]);
 close(fd[1]);
 close(fd[2]);
 
 append_files(files, fd1);

 close(fd1);

 system("rm -r main.tmp");
 system("rm -r let.tmp");
 system("rm -r prop.tmp");


}
;

%%

int main(int argc, char **argv){
  
  FILE * glade;
  extern FILE * yyin;

  if(argc > 1){

    glade = fopen(argv[1], "r");

    if(!glade){
      
      printf("Error, could not open %s", argv[1]);
      exit(1);
    }
    
    yyin = glade;
  }
  yyparse();

  return 0;	
}

int yyerror(char * s){
  printf("%s\n", s);
  return 0;
}
















