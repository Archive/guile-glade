#include <stdio.h>


#ifndef GTK_LIBRARY
#define GTK_LIBRARY
#include "gtkobjs.h"
#endif


void write_box(void *, int *, char **);
void write_button(void *, int *, char **);
void write_entry(void *, int *, char **);
void write_text(void *, int *, char **);
void write_window(void *, int *, char **);


void write_window(void * param, int * fd, char ** stack){

  GtkWindow * window = (GtkWindow *) param;
  int let, prop;

  let = fd[1];
  prop = fd[2];

  write(let, "(", 1);
  write(let, window->name, strlen(window->name));
  write(let, " ", 1);
  write(let, " (gtk-window-new", 16); 
  
  if(!(strcmp("GTK_WINDOW_TOPLEVEL", window->type)))
    write(let, " 'dialog)",9);
  else
    write(let, " 'toplevel)", 11);

  write(let, ")\n", 2);

  write(prop, "\n\t(gtk-window-set-title ", 24);
  write(prop, window->name, strlen(window->name));
  write(prop, " \"", 2);
  write(prop, window->title, strlen(window->title));
  write(prop, "\") \n", 4);
}


void write_box(void * param, int * fd, char ** stack){

  GtkBox * box = (GtkBox *) param;
  char num[4];
  int let, prop, index;

  let = fd[1];
  prop = fd[2];
  index = fd[3];

  write(let, "\t(", 2);
  write(let, box->name, strlen(box->name));
  //write(let, "(", 1);

  if(!(box->class))
    write(let, "(gtk-vbox-new ", 14);
  else
    write(let, "(gtk-hbox-new ", 14);

  if(box->homogeneous)
    write(let, "#t ", 3);
  else 
    write(let, "#f ", 3);

  // we put the numerical value of spacing into the num buffer

  sprintf(num, "%d", box->spacing);  

  write(let, num, strlen(num));
  write(let, "))\n", 3);

  /*

    In this part we check whether to add the box into another box
    or do an add container between window and box

  */
    if(!(index-1)){
      write(prop, "\t(gtk-container-add ", 20); 
      write(prop,  stack[0], strlen(stack[0]));
      write(prop, " ", 1);
      write(prop, stack[index], strlen(stack[index]));
      write(prop, ")\n", 2);
    }
    else {
      write(prop, "(gtk-box-pack-start ", 20);
      write(prop, stack[index-1], strlen(stack[index-1]));
      write(prop, " ", 1);
      write(prop, stack[index], strlen(stack[index]));
      write(prop, ")\n", 2);
    }
  
 }

void write_button(void * param, int * fd, char ** stack){

  GtkButton * button = (GtkButton *) param;
  int let, prop, index;
 
  let = fd[1];
  prop = fd[2];
  index = fd[3];

  write(let, "\t(", 2);
  write(let, button->name, strlen(button->name));
  write(let, " ", 1);
  write(let, "(gtk-button-new-with-label ", 27);
  write(let, "\"", 1);
  write(let, button->label, strlen(button->label));
  write(let, "\")) ", 4);
  //write(let, ")\n", 2);
    
  write(prop, "\t(gtk-box-pack-start ", 21);
  write(prop, stack[index], strlen(stack[index]));
  write(prop, " ", 1);
  write(prop, button->name, strlen(button->name));
  write(prop, " ", 1);
  
  if(button->expand)
    write(prop, "#t ", 2);
  else  
    write(prop, "#f ", 2);

  write(prop, " ", 1);

  if(button->fill)
    write(prop, "#t) \n", 3);
  else
    write(prop, "#f) \n", 3);
 
}

void write_entry(void * param, int * fd, char ** stack){

  GtkEntry * entry = (GtkEntry *) param;
  int let, prop, index;
  char num[BUFSIZ];
  let = fd[1];
  prop = fd[2];
  index = fd[3];

  write(let, "\t(", 2);
  write(let, entry->name, strlen(entry->name));
  write(let, " ", 1);
  if(!(entry->text_max_length))
    write(let, "(gtk-entry-new)) \n ", 18);
  else {
    write(let, "(gtk-entry-new-with-max-length ", 31); 
    sprintf(num, "%d", entry->text_max_length);
    write(let, num, strlen(num));
    write(let, ")) \n", 4);
  }
  if(!(entry->editable)){
    write(prop, "\t(gtk-entry-set-editable ", 25);
    write(prop, entry->name, strlen(entry->name));
    write(prop, " #f) \n", 6);
  }

  if(!(entry->text_visible)){
    write(prop, "\t(gtk-entry-set-visibility ", 27);
    write(prop, entry->name, strlen(entry->name));
    write(prop, " #f) \n", 6);
  } 

  write(prop, "\t(gtk-box-pack-start ", 21);
  write(prop, stack[index], strlen(stack[index]));
  write(prop, " ", 1);
  write(prop, entry->name, strlen(entry->name));
  write(prop, " ", 1);
  
  (entry->expand)? write(prop, "#t ", 2) : write(prop, "#f ", 2);

  write(prop, " ", 1);
  
  (entry->fill)? write(prop, "#t) \n", 3) : write(prop, "#f) \n", 3);
  
}

void write_text(void * param, int * fd, char ** stack){

  GtkText * text = (GtkText *) param;
  int let, prop, index;
  
  let = fd[1];
  prop = fd[2];
  index = fd[3];

  write(let, "\t(", 2);
  write(let, text->name, strlen(text->name));

  /*
    Obviously this will need to be revised for
    it can't accept other scrolls...
  */
  write(let, " (gtk-text-new #f #f)) \n", 23);
  
  
  /*
    I wonder if all this cheese can be put in a function...
  */
  write(prop, "\t(gtk-box-pack-start ", 21);
  write(prop, stack[index], strlen(stack[index]));
  write(prop, " ", 1);
  write(prop, text->name, strlen(text->name));
  write(prop, " ", 1);
  
  (text->expand)? write(prop, "#t ", 2) : write(prop, "#f ", 2);

  write(prop, " ", 1);
  
  (text->fill)? write(prop, "#t) \n", 3) : write(prop, "#f) \n", 3);


  if(!(text->editable)){
    write(prop, "\t(gtk-text-set-editable ", 24);
    write(prop, text->name, strlen(text->name));
    write(prop, " #f) \n", 6);
  }
}
















