#include <stdio.h>

/*
This file contains strings so we can use them later.
This set the properties for the guile file.
Eg:

;;Created by guile-glade
;;Ariel Rios 1999

(use-modules (gtk gtk) (gtk gdk))

*/
     
char * COPYRIGHT = ";;Created by guile-glade.\n;;Ariel Rios 1999\n";

char * GTK_ONLY = "\n(use-modules (gtk gtk) (gtk gdk))\n"; 

char * GNOME = "\n(use-modules (gtk gtk) (gtk gdk) (gnome gnome))\n";

char * GUILE_DEFINE = "\n(define (";

char * OPEN_PARENTHESIS = "(";

char * CLOSE_PARENTHESIS = ")";

char * LET = "\t(let (";

char * LETS = "\n  (let* (";

char * GTKWINDOW_NEW = "\n\t(gtk-window-new";   










